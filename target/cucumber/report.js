$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/Features/weatherForecast.feature");
formatter.feature({
  "line": 1,
  "name": "Weather Forecast",
  "description": "",
  "id": "weather-forecast",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 5,
  "name": "Weather-Forecast",
  "description": "",
  "id": "weather-forecast;weather-forecast",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Open the weather forecast website",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Search the city",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "Fetch and report in html file",
  "keyword": "Then "
});
formatter.match({
  "location": "WeatherForecast.open_the_weather_forecast_website()"
});
formatter.result({
  "duration": 20066700500,
  "status": "passed"
});
formatter.match({
  "location": "WeatherForecast.search_the_city()"
});
formatter.result({
  "duration": 3178839500,
  "status": "passed"
});
formatter.match({
  "location": "WeatherForecast.fetch_and_report_in_html_file()"
});
formatter.result({
  "duration": 4353627100,
  "status": "passed"
});
});